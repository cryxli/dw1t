<?php
session_start();

require_once('lib/settings.php');
require_once('lib/login-api.php');

// Google passes a parameter 'code' in the Redirect Url
if(isset($_GET['code'])) {
	try {
		$gapi = new LoginApi();
		
		// Get the access token 
		$data = $gapi->getAccessToken(GOOGLE_TOKEN_URL, GOOGLE_CLIENT_ID, GOOGLE_CLIENT_REDIRECT_URL, GOOGLE_CLIENT_SECRET, $_GET['code']);
		
        //echo '<pre>';print_r($data); echo '</pre>';

		// Get user information
		$user_info = $gapi->getUserProfileInfo(GOOGLE_USERINFO_URL, $data['access_token']);

        //echo '<pre>';print_r($user_info); echo '</pre>';

		// Now that the user is logged in you may want to start some session variables
		// $_SESSION['logged_in'] = 1;
        $_SESSION['username'] = $user_info['displayName'];

        $index = 0;
        while($user_info['emails'][$index]['type'] != 'account') {
            $index++;
        }
        $_SESSION['email'] = $user_info['emails'][$index]['value'];
        $_SESSION['hash'] = md5($_SESSION['email'] . "/" . $_SESSION['username']);

		// You may now want to redirect the user to the home page of your website
		header('Location: user.php');

	} catch(Exception $e) {
		echo $e->getMessage();
        header("Location: index.php");
	}
} else {
    header("Location: index.php");
}
?>