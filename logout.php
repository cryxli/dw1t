<?php
session_start();
unset($_SESSION['username'], $_SESSION['email'], $_SESSION['hash']);
header("Location: index.php");
?>