<?php
session_start();
$loggedIn = isset($_SESSION['username']);
?><!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Public Dragon Warrior 1 Randomizer Tracker</title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  <link rel="stylesheet" href="css/main.css">
  <link rel="icon" href="image/dragonlord.png">
</head>
<body>
    <?php include('lib/nav.php'); ?>

    <div class="container">
        <div class="jumbotron">
            <h1>Dragon Warrior 1 Randomizer Tracker</h1>
            <p>Track item and quest progression while playing a DW1 randomizer, or, have someone else track it for you.</p>
        </div>
    </div>

    <footer class="page-footer font-small">
        <div class="footer-copyright text-center py-3">Powered by <a href="http://cryx.li/">cryx.li</a></div>
    </footer>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
</body>
</html>