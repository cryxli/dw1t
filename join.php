<?php
session_start();
$loggedIn = isset($_SESSION['username']);


?><!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Public Dragon Warrior 1 Randomizer Tracker</title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  <link rel="stylesheet" href="css/tracker.css">
  <link rel="icon" href="image/dragonlord.png">
<style>
/*
.code {
    width: auto;
    text-align: center;
    font-size: 200%;
    font-weight: bold;
}
*/
.code {
    text-align: center;
}
.code .input-group {
    display: inline-flex;
    width: auto;
}
.code .input-group .btn,
.code .input-group .form-control {
    width: auto;
    text-align: center;
    font-size: 200%;
    font-weight: bold;
}
.form-control::-webkit-input-placeholder { /* Chrome/Opera/Safari */ color: #ddd; }
.form-control::-moz-placeholder { /* Firefox 19+ */ color: #ddd; }
.form-control:-ms-input-placeholder { /* IE 10+ */ color: #ddd; }
.form-control:-moz-placeholder { /* Firefox 18- */ color: #ddd; }
input[type="image"] { width: 62px; height: 62px; }
</style>
</head>
<body>
<?php include('lib/nav.php'); ?>

<div class="container">

    <form action="dw1t.php" method="GET">
        <div class="form-group" style="margin-top: 2rem;">
            <p>To join a tracker, please enter the code from the top left of the tracker page. If you came here from scanning a QR code, the tracker most likely has expired.</p>
        </div>

        <div class="code">
            <div class="input-group">
                <input type="text" maxlength="4" size="4" name="tracker" class="form-control" placeholder="ABCD" />
                <div class="input-group-append">
                    <!-- <input type="submit" class="btn" value="join"/> -->
                    <input type="image" class="btn btn-secondary" src="image/join.png" alt="join" />
                </div>
            </div>
        </div>

        <div id="error" class="form-group" style="display: none; margin-top: 3rem;">
            <div class="alert alert-danger" role="alert">There is no tracker to join.</div>
        </div>
    </form>

    <div class="modal" id="alert" tabindex="-1" role="dialog" aria-labelledby="alertTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="alertTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="btn-group-vertical d-flex" role="group"></div>
                </div>
            </div>
        </div>
    </div>
</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
  <script src="js/join.js"></script>
</body>
</html>