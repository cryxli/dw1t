
var session = null;

var weapon = [
    {name: "no weapon", img: "weapon_7.png"},
    {name: "Bamboo Pole", img: "weapon_1.png"},
    {name: "Club", img: "weapon_2.png"},
    {name: "Copper Sword", img: "weapon_3.png"},
    {name: "Hand Axe", img: "weapon_4.png"},
    {name: "Broad Sword", img: "weapon_5.png"},
    {name: "Flame Sword", img: "weapon_6.png"},
    {name: "Erdrick's Sword", img: "weapon_7.png"}
];

var armor = [
    {name: "no armor", img: "armor_7.png"},
    {name: "Clothes", img: "armor_1.png"},
    {name: "Leather Armor", img: "armor_2.png"},
    {name: "Chain Mail", img: "armor_3.png"},
    {name: "Half Plate", img: "armor_4.png"},
    {name: "Full Plate", img: "armor_5.png"},
    {name: "Magic Armor", img: "armor_6.png"},
    {name: "Erdrick's Armor", img: "armor_7.png"}
];

var shield = [
    {name: "no shield", img: "shield_3.png"},
    {name: "Small Shield", img: "shield_1.png"},
    {name: "Large Shield", img: "shield_2.png"},
    {name: "Silver Shield", img: "shield_3.png"}
];

var selectWeapon = function() {
    var index = parseInt($('#weapon').attr('data-selected')||'0');
    showItemPopup('Select Weapon', weapon, index, function() {
        selected = $(this).attr('data-selected');
        $('#weapon img').attr('src', 'image/' + weapon[selected].img);
        $('#weapon img').attr('title', weapon[selected].name);
        $('#weapon').attr('data-selected', selected);
        $('#alert').modal('hide');
        putState('weapon', selected);
    });
};

var selectArmor = function() {
    var index = parseInt($('#armor').attr('data-selected')||'0');
    showItemPopup('Select Armor', armor, index, function() {
        var index = $(this).attr('data-selected');
        $('#armor img').attr('src', 'image/' + armor[index].img);
        $('#armor img').attr('title', armor[index].name);
        $('#armor').attr('data-selected', index);
        $('#alert').modal('hide');
        putState('armor', index);
    });
};

var selectShield = function() {
    var index = parseInt($('#shield').attr('data-selected')||'0');
    showItemPopup('Select Shield', shield, index, function() {
        var index = $(this).attr('data-selected');
        $('#shield img').attr('src', 'image/' + shield[index].img);
        $('#shield img').attr('title', shield[index].name);
        $('#shield').attr('data-selected', index);
        $('#alert').modal('hide');
        putState('shield', index);
    });
};

var toggleItem = function() {
    var sel = parseInt($(this).attr('data-selected')||'0');
    sel = (sel + 1) % 2;
    $(this).attr('data-selected', sel);
    putState($(this).attr('id'), sel);
};

var showItemPopup = function(title, dataArrays, selected, callback) {
    $('#alertTitle').html(title);
    var s = '';
    $.each(dataArrays, function(index, elem) {
        s += '<button class="btn ';
        if (index == selected) {
            s += 'btn-secondary';
        } else {
            s += 'btn-outline-secondary';
        }
        s+= '" data-selected="' + index + '">';
        s += '<img src="image/' + elem.img + '" /> ' + elem.name;
        s += '</button>';
    });
    $('#alert .btn-group-vertical').html(s);
    $('#alert .btn-group-vertical button').click(callback);
    $('#alert').modal('show');
};

var pollState = function() {
    $.get('/api/' + session).done(function(resp) {
        $.each(resp, function( key, value ) {
            if (key != 'hash') {
                $('#' + key).attr('data-selected', value);
                var arr = null;
                if (key == 'weapon') {
                    arr = weapon;
                } else if (key == 'armor') {
                    arr = armor;
                } else if (key == 'shield') {
                    arr = shield;
                }
                if (!!arr) {
                    $('#' + key + ' img').attr('src', 'image/' + arr[value].img);
                    $('#' + key + ' img').attr('title', arr[value].name);
                }
            }
        });
    });
};

var putState = function(field, value) {
    $.ajax({
        url: '/api/' + session,
        type: 'PUT',
        data: {"field": field, "value" : value}
    });
};

var showLink = function() {
    $('#alertTitle').html($('body').attr('data-tracker'));
    $('#alertMsg').html('<div id="qr" style="margin: auto;"></div>');
    new QRCode(document.getElementById("qr"), location.href);
    $('#alert').modal();
}

$(document).ready(function() {
    $('div.item').attr('data-selected', '0');

    $('#weapon img').attr('title', weapon[0].name);
    $('#armor img').attr('title', armor[0].name);
    $('#shield img').attr('title', shield[0].name);

    $('div.item.toggle').click(toggleItem);
    $('#weapon').click(selectWeapon);
    $('#armor').click(selectArmor);
    $('#shield').click(selectShield);

    session = $('nav .navbar-brand').text();

    pollState();
    window.setInterval(pollState, 10000);

    $('nav button').click(showLink);
});
