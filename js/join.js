$(document).ready(function() {
    $('input[name="tracker"]').focus();
    $('input[name="tracker"]').bind('input blur',function(){
        var node = $(this);
        node.val(node.val().replace(/[^A-Za-z]/g,'').toUpperCase());
    });
    $('input[name="tracker"]').on("keydown", function(event){
        // Allow controls such as backspace, tab etc.
        var arr = [13,8,9,16,17,20,35,36,37,38,39,40,45,46];

        // Allow letters
        for(var i = 65; i <= 90; i++){
            arr.push(i);
        }

        // Prevent default if not in array
        if(jQuery.inArray(event.which, arr) === -1){
            event.preventDefault();
        }
    });
    $('form').submit(function() {
        $('#error').hide();
        $.get('/api/' + $('input[name="tracker"]').val()).done(function() {
            window.location.href = 'dw1t.php?tracker=' + $('input[name="tracker"]').val();
        }).fail(function() {
            $('#error').show();
            $('input[name="tracker"]').val('');
            $('input[name="tracker"]').focus();
        });
        return false;
    });
});