$(document).ready(function() {
    $('table button').click(function() {
        var row = $(this).closest('tr');
        var tracker = row.find('td a').text();
        $.ajax({
            url: '/api/' + tracker,
            type: 'DELETE'
        }).done(function(resp) {
            if (row.siblings().length == 1) {
                row.closest('table').append('<tr><td colspan="4">No active trackers found.</td></tr>');
            }
            row.remove();
        }).fail(function() {
            // TODO
            alert('Could not delete tracker.');
        });
    });
});