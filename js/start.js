$(document).ready(function() {
    $('#start').click(startTracker);
});

var startTracker = function() {
    $.ajax({
        url: 'start.php',
        type: 'POST'
    }).done(function(resp) {
        location.href = 'dw1t.php?tracker=' + resp.tracker;
    }).fail(function() {
        // TODO
        alert('Could not create a tracker.');
    });
};