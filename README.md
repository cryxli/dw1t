# Public Dragon Warrior 1 Tracker

There is an amazing [Dragon Warrior 1](https://en.wikipedia.org/wiki/Dragon_Quest_(video_game)) [Rando Tracker](https://jsnkuhn.github.io/DWRandomizerTracker/) by [jsnkuhn](https://github.com/jsnkuhn/jsnkuhn.github.io). But it is designed as a single user stateless application. You cannot start a tracker and "remote control" it from your phone, as an example.

This is what DW1T tries to address. A tracker can be controlled by anybody that knows its 4 character tracker code.

## Setup

- Deploy everything on a Apache/PHP host.
- Create a valid `lib/settings.php` following the example of `lib/dist.settings.php`.
- Ensure that PHP can write to the `data` directory.

## Demo

There is a [Demo](http://dw1t.cryx.li/) installation that supports Google and GitHub login. Only logged in people can create new trackers. Joining is open for everyone.
