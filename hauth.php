<?php
session_start();

require_once('lib/settings.php');
require_once('lib/login-api.php');

if(isset($_GET['code']) and isset($_GET['state'])) {
	try {
		$gapi = new LoginApi();

		// Get the access token
		$data = $gapi->getAccessToken(GITHUB_TOKEN_URL, GITHUB_CLIENT_ID, GITHUB_CLIENT_REDIRECT_URL, GITHUB_CLIENT_SECRET, $_GET['code'], $_GET['state']);

        //echo '<pre>Access Token<br/>';print_r($data); echo '</pre>';

		// Get user information
		$user_info = $gapi->getUserProfileInfo(GITHUB_USERINFO_URL, $data['access_token']);

        //echo '<pre>User<br/>';print_r($user_info); echo '</pre>';

		// Now that the user is logged in you may want to start some session variables
		// $_SESSION['logged_in'] = 1;
        $_SESSION['username'] = $user_info['name'];
        // no email, use login
        $_SESSION['email'] = $user_info['login'];
        $_SESSION['hash'] = md5($_SESSION['email'] . "/" . $_SESSION['username']);

		// You may now want to redirect the user to the home page of your website
		header('Location: user.php');

	} catch(Exception $e) {
		echo $e->getMessage();
	    header("Location: index.php");
    }
} else {
    header("Location: index.php");
}
?>