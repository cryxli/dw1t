<?php
# --------------------------------------------------------------------
# This file is an example how the real "settings.php" could look like.
# When installed on a domain, these settings must match their related
# settings with Google and GitHub.
# --------------------------------------------------------------------

/* Google App Client ID */
define('GOOGLE_CLIENT_ID', ''); // <---

/* Google App Client Secret */
define('GOOGLE_CLIENT_SECRET', ''); // <---

/* Google App Redirect URL */
define('GOOGLE_CLIENT_REDIRECT_URL', 'http://' . $_SERVER["HTTP_HOST"] . '/gauth.php');

define('GOOGLE_TOKEN_URL', 'https://accounts.google.com/o/oauth2/token');

define('GOOGLE_USERINFO_URL', 'https://www.googleapis.com/plus/v1/people/me?fields=displayName%2Cemails');


/* GitHub App Client ID */
define('GITHUB_CLIENT_ID', ''); // <---

/* GitHub App Client Secret */
define('GITHUB_CLIENT_SECRET', ''); // <---

/* GitHub App Redirect URL */
define('GITHUB_CLIENT_REDIRECT_URL', 'http://' . $_SERVER["HTTP_HOST"] . '/hauth.php');

define('GITHUB_TOKEN_URL', 'https://github.com/login/oauth/access_token');

define('GITHUB_USERINFO_URL', 'https://api.github.com/user');

?>