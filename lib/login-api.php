<?php

class LoginApi {

	public function getAccessToken($url, $client_id, $redirect_uri, $client_secret, $code, $state = NULL) {
		$curlPost = 'accept=json' . '&client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code='. $code . '&grant_type=authorization_code';
        if (!is_null($state)) {
            // because of GitHub
            $curlPost = $curlPost . '&state=' . $state;
        }
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_POST, 1);		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        // because of GitHub
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Accept: application/json' ));
        $raw = curl_exec($ch);
        //echo "<pre>$raw</pre>";
		$data = json_decode($raw, true);
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
		if($http_code != 200) {
			throw new Exception('Error : Failed to receieve access token');
        }
		return $data;
	}

	public function getUserProfileInfo($url, $access_token) {
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer '. $access_token,
            // because of GitHub
            'Accept: application/json'
        ));
        // because of GitHub
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $raw = curl_exec($ch);
        // echo "<pre>$raw</pre>";
		$data = json_decode($raw, true);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);		
		if($http_code != 200) {
			throw new Exception('Error : Failed to get user information');
        }
		return $data;
	}
}

?>