<?php

function noCacheHeaderJson() {
    noCacheHeader('application/json');
}

function noCacheHeader($mimeType = '') {
    if ($mimeType != '') {
        header("Content-Type: $mimeType");
    }
    // no cache
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
}

function readUserIndex($trackerToDelete = '0000') {
    $trackers = array();
    $filename = "data/" . $_SESSION['hash'] . ".json";
    if (file_exists($filename)) {
        $jsondata = file_get_contents($filename);
        $obj = json_decode($jsondata, true);
        $changed = false;
        foreach($obj as $value) {
            if (
                $value['tracker'] == $trackerToDelete
                or time() > $value['created'] + 24 * 60 * 60
            ) {
                // remove tracker
                if (@unlink("data/" . $value['tracker'] . ".json")) {
                    $changed = true;
                }
            } else {
                array_push($trackers, $value);
            }
        }
        if ($changed) {
            $serialised = json_encode($trackers);
            file_put_contents($filename,$serialised);
        }
    }
    return $trackers;
}

function loadTrackerFile($tracker) {
    $jsondata = file_get_contents("data/$tracker.json");
    return json_decode($jsondata, true);
}

function saveTrackerFile($tracker, $trackerFile) {
    $serialised = json_encode($trackerFile);
    file_put_contents("data/$tracker.json", $serialised);
}

function addTrackerToUser($tracker) {
    $userFile = readUserIndex();
    array_push($userFile, array("tracker" => $tracker, "created" => time()));
    $userfile = "data/" . $_SESSION['hash'] . ".json";
    file_put_contents($userfile, json_encode($userFile));
}

function generateTrack($offset = 0) {
    $seed = time() . $_SESSION['hash'] . $offset;
    $hash = abs(crc32($seed));
    $i = $hash % 456976;

    $d = $i % 26;
    $c = floor($i/26) % 26;
    $b = floor($i/26/26) % 26;
    $a = floor($i/26/26/26) % 26;
    $tracker = chr(ord('A')+$a) . chr(ord('A')+$b) . chr(ord('A')+$c) . chr(ord('A')+$d);

    if (!file_exists("data/$tracker.json")) {
        return $tracker;
    } else if ($offset < 10) {
        return generateTrack($offset+1);
    } else {
        return NULL;
    }
}

?>