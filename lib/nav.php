<?php

function endsWith($haystack, $needle) {
    $length = strlen($needle);
    return $length === 0 || (substr($haystack, -$length) === $needle);
}
function active($page) {
    if (endsWith($_SERVER['SCRIPT_FILENAME'], $page)) {
        return ' active';
    } else {
        return '';
    }
}

$loggedIn = isset($_SESSION['username']);

$trackerId = "DW1T";
if (isset($_GET['tracker'])) {
    $trackerId = $_GET['tracker'];
}

?>    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="index.php"><?=$trackerId ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigationbar" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navigationbar">
        <div class="navbar-nav">
          <?php if ($loggedIn) {
          ?><a class="nav-item nav-link<?=active('start.php') ?>" href="start.php">Start</a><?php
          } ?>
          <a class="nav-item nav-link<?=active('join.php') ?>" href="join.php">Join</a>
          <?php if (!$loggedIn) {
          ?><a class="nav-item nav-link<?=active('login.php') ?>" href="login.php">Login</a><?php
          } else {
          ?><a class="nav-item nav-link<?=active('user.php') ?>" href="user.php">Account</a><?php
          } ?>
        </div>
      </div>
    </nav>
