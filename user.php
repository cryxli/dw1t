<?php
session_start();
if (!isset($_SESSION['username'])) {
    header("Location: index.php");
    die();
}

require_once('lib/common.php');
$trackers = readUserIndex();

?><!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Public Dragon Warrior 1 Randomizer Tracker</title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  <link rel="stylesheet" href="css/main.css">
  <link rel="icon" href="image/dragonlord.png">
</head>
<body>
    <?php include('lib/nav.php'); ?>

    <div class="container">

        <div class="row justify-content-end" style="margin-bottom: 1rem;">
            <a href="logout.php" class="btn btn-danger">Logout</a>
        </div>

        <table class="table">
        <tr>
            <th>Tracker</th>
            <th>Created</th>
            <th>Expires</th>
            <th>Action</th>
        </tr><?php
if (count($trackers) > 0) {
    foreach($trackers as $t) {
        ?><tr>
            <td><a href="dw1t.php?tracker=<?=$t['tracker'] ?>"><?=$t['tracker'] ?></a></td>
            <td><?=date('Y-m-d H:i:s', $t['created']) ?></td>
            <td><?php
                $delta = $t['created'] + 24 * 60 * 60 - time();
                $m = floor($delta / 60) % 60;
                $h = floor($delta / 60 / 60);
                if ($h > 0) {
                    echo $h."h".$m."m";
                } else {
                    echo $m."m";
                }
            ?></td>
            <td><button class="btn btn-outline-secondary btn-sm">Delete</button></td>
        </tr><?php
    }
} else { ?><tr>
            <td colspan="4">No active trackers found.</td>
        </tr><?php } ?>
        </table>

    </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
  <script src="js/user.js"></script>
</body>
</html>