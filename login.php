<?php
session_start();
if (isset($_SESSION['username'])) {
    header("Location: user.php");
    die();
}

require_once('lib/settings.php');
$google_login_url = 'https://accounts.google.com/o/oauth2/v2/auth'
    . '?scope=' . urlencode('https://www.googleapis.com/auth/userinfo.email')
    . '&redirect_uri=' . urlencode(GOOGLE_CLIENT_REDIRECT_URL)
    . '&response_type=code'
    . '&client_id=' . GOOGLE_CLIENT_ID
    . '&access_type=online'
;

$github_login_url = 'https://github.com/login/oauth/authorize'
    . '?scope=user:email'
    . '&redirect_uri=' . urlencode(GITHUB_CLIENT_REDIRECT_URL)
    . '&client_id=' . GITHUB_CLIENT_ID
    . '&state=' . str_shuffle(MD5(microtime()))
;

?><!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Public Dragon Warrior 1 Randomizer Tracker</title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  <link rel="stylesheet" href="css/main.css">
  <link rel="icon" href="image/dragonlord.png">
</head>
<body>
    <?php include('lib/nav.php'); ?>

    <div class="container">
        <div class="row justify-content-md-center mt-4">
            <div class="col-md-auto">
                <a href="<?= $google_login_url ?>" class="btn btn-danger">Login with Google</a>
            </div>
        </div>
        <div class="row justify-content-md-center mt-4">
            <div class="col-md-auto">
                <a href="<?= $github_login_url ?>" class="btn btn-dark">Login with GitHub</a>
            </div>
        </div>
    </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
</body>
</html>