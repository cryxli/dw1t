<?php
session_start();

$method = $_SERVER['REQUEST_METHOD'];
$loggedIn = isset($_SESSION['username']);

if (isset($_GET['tracker'])) {
    if (strlen($_GET['tracker']) == 4) {
        $tracker = strtoupper($_GET['tracker']);
    }
} else if (isset($_SERVER['REQUEST_URI']) and substr($_SERVER['REQUEST_URI'], 0, 5) == '/api/') {
    if (strlen($_SERVER['REQUEST_URI']) == 9) {
        $tracker = strtoupper(substr($_SERVER['REQUEST_URI'], 5));
    }
}
$filename = "data/$tracker.json";
if (!isset($tracker)) {
    die();
}

require_once('lib/common.php');

if ($method == 'PUT' and !file_exists($filename) and $loggedIn) {
    // create tracker
    $trackerFile = loadTrackerFile('template');
    $obj['hash'] = $_SESSION['hash'];
    saveTrackerFile($tracker, $trackerFile);

    // add tracker to user
    addTrackerToUser($tracker);

    // serve tracker file
    serveString(json_encode($trackerFile));

} elseif ($method == 'PUT' and file_exists($filename)) {
    $params = file_get_contents("php://input");
    parse_str($params, $fieldValue);
    if (isset($fieldValue['field']) and isset($fieldValue['value'])) {
        // save changes
        $trackerFile = loadTrackerFile($tracker);
        $trackerFile[$fieldValue['field']] = intval($fieldValue['value']);
        saveTrackerFile($tracker, $trackerFile);

        serveString(json_encode($trackerFile));
    } else {
        serveFile($filename);
    }

} else if ($loggedIn and $method == 'DELETE') {
    readUserIndex($tracker);
    echo "ok";

} else if ($method == 'GET') {
    serveFile($filename);

} else {
    http_response_code(404);
}
die();

function serveString($serialised) {
    noCacheHeaderJson();
    header('Content-Length: ' . strlen($serialised));
    echo $serialised;
}

function serveFile($filename) {
    noCacheHeaderJson();
    header('Content-Length: ' . filesize($filename));
    readfile($filename);
}

?>