<?php
session_start();

if (!isset($_GET['tracker'])) {
    header("Location: index.php");
    die();
}
$tracker = strtoupper($_GET['tracker']);
if (strlen($tracker) != 4) {
    header("Location: index.php");
    die();
}

$_SESSION['tracker'] = $tracker;
?><!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Public Dragon Warrior 1 Randomizer Tracker</title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  <link rel="stylesheet" href="css/dw1t.css">
  <link rel="icon" href="image/dragonlord.png">
</head>
<body data-tracker="<?=$tracker?>">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <span class="navbar-brand"><a href="user.php"><?php echo $_SESSION['tracker']; ?></a></span>
    <button class="btn btn-outline-secondary btn-sm">QR</button>
</nav>

<div class="container">

    <div class="row">
        <div class="items mx-auto">
            <div class="header">Items</div>
            <div>
                <div id="staff-of-rain" class="item toggle"><img src="image/staff_of_rain.png" title="Staff of Rain" /></div>
                <div id="silver-harp" class="item toggle"><img src="image/silver-harp.png" title="Silver Harp" /></div>
                <div id="magic-key" class="item toggle"><img src="image/magic-key.png" title="Magic Key" /></div>
                <div id="fighter-ring" class="item toggle"><img src="image/fighter-ring.png" title="Fighter's Ring" /></div>
                <div id="weapon" class="item"><img src="image/weapon_7.png" /></div>
            </div><div>
                <div id="stone-of-sunlight" class="item toggle"><img src="image/stone-of-sunlight.png" title="Stone of Sunlight" /></div>
                <div id="rainbow-drop" class="item toggle"><img src="image/rainbow-drop.png" title="Rainbow Drop" /></div>
                <div id="ball-of-light" class="item toggle"><img src="image/ball-of-light.png" title="Ball of Light" /></div>
                <div id="dragon-scale" class="item toggle"><img src="image/dragon_scale.png" title="Dragon's Scale" /></div>
                <div id="armor" class="item"><img src="image/armor_7.png" /></div>
            </div><div>
                <div id="token" class="item toggle"><img src="image/token.png" title="Erdrick's Token" /></div>
                <div id="fairy-flute" class="item toggle"><img src="image/fairy-flute.png" title="Fairy Flute" /></div>
                <div id="gps" class="item toggle"><img src="image/gwaelin.png" title="Gwaelin's Love" /></div>
                <div id="death-necklace" class="item toggle"><img src="image/death_necklace.png" title="Death Necklace" /></div>
                <div id="shield" class="item"><img src="image/shield_3.png" /></div>
            </div>
            <div class="header">Spells</div>
            <div>
                <div id="heal" class="item toggle"><img src="image/heal.png" title="Heal" /></div>
                <div id="hurt" class="item toggle"><img src="image/hurt.png" title="Hurt" /></div>
                <div id="sleep" class="item toggle"><img src="image/sleep.png" title="Sleep" /></div>
                <div id="radiant" class="item toggle"><img src="image/radiant.png" title="Radiant" /></div>
                <div id="stopspell" class="item toggle"><img src="image/stopspell.png" title="Stop Spell" /></div>
            </div><div>
                <div id="outside" class="item toggle"><img src="image/outside.png" title="Outside" /></div>
                <div id="return" class="item toggle"><img src="image/return.png" title="Return" /></div>
                <div id="repel" class="item toggle"><img src="image/repel.png" title="Repel" /></div>
                <div id="healmore" class="item toggle"><img src="image/healmore.png" title="Heal More" /></div>
                <div id="hurtmore" class="item toggle"><img src="image/hurtmore.png" title="Hurt More" /></div>
            </div>
        </div>
    </div>

    <div class="modal" id="alert" tabindex="-1" role="dialog" aria-labelledby="alertTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="alertTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="btn-group-vertical d-flex" role="group" id="alertMsg"></div>
          </div>
        </div>
      </div>
    </div>

</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
  <script src="js/qrcode.min.js"></script>
  <script src="js/dw1t.js"></script>
</body>
</html>