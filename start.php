<?php
session_start();
$method = $_SERVER['REQUEST_METHOD'];
$loggedIn = isset($_SESSION['username']);
if (!$loggedIn) {
    header("Location: join.php");
    die();
} else if ($method == 'POST') {
    require_once('lib/common.php');
    $tracker = generateTrack();
    if (!is_null($tracker)) {
        $trackerFile = loadTrackerFile('template');
        $trackerFile['hash'] = $_SESSION['hash'];
        saveTrackerFile($tracker, $trackerFile);
        addTrackerToUser($tracker);

        noCacheHeaderJson();
        echo '{"tracker": "' . $tracker . '"}';
    } else {
        http_response_code(507);
    }
    die();
}
?><!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Public Dragon Warrior 1 Randomizer Tracker</title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  <link rel="stylesheet" href="css/tracker.css">
  <link rel="icon" href="image/dragonlord.png">
</head>
<body>
<?php include('lib/nav.php'); ?>

<div class="container">

    <div class="row mt-4">
        <p>Hit the button below to create a new tracker. When on the tracker page, give the tracker code in the top left corner to your friends to have them watch or update the tracker.</p>
    </div>
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <button class="btn btn-outline-primary" id="start">Start a new tracker</button>
        </div>
    </div>
    <div class="row mt-4">
        <p>Depending on the server load this may fail. Try again and delete old trackers on your <a href="user.php">Account</a> page.</p>
    </div>

    <div class="modal" id="alert" tabindex="-1" role="dialog" aria-labelledby="alertTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="alertTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="btn-group-vertical d-flex" role="group"></div>
                </div>
            </div>
        </div>
    </div>
</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
  <script src="js/start.js"></script>
</body>
</html>